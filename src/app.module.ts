import { Module } from "@nestjs/common";
import { PositionVehiculeModule } from "./position-vehicule/position-vehicule.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { LastInfo } from "./position-vehicule/entities/last-info";
import { Vehicule } from "./position-vehicule/entities/vehicule";

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: "postgres",
      host: "autorun-database-dev2.cx0fcdpndddc.eu-west-3.rds.amazonaws.com",
      // host: "localhost",
      port: 5432,
      username: "postgres",
      // password: "TDMPROJECT",
      password: "gdMHCk2pap5Zz95",
      database: "autorun",
      synchronize: false, // false for production
      autoLoadEntities: true,
      logging: true,
      entities: [LastInfo, Vehicule]
    }), PositionVehiculeModule]
})
export class AppModule {
}
